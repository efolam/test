<?php

namespace App\Http\Controllers;

use App\Mail\Welcome;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Страница регистрации
     *
     * @return View
     */
    public function index(): View
    {
        return view('register');
    }

    /**
     * Регистрация пользователя
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function register(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name'       => ['required', 'string', 'max:255'],
            'email'      => ['required', 'email', 'max:255', 'unique:users'],
            'phone'      => ['required'],
            'country_id' => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $user = User::create([
            'name'     => $request['name'],
            'email'    => $request['email'],
            'password' => Hash::make('secret'),
        ]);

        $user->countries()->sync([
            $request['country_id']
        ]);

        $user->phones()->create([
            'phone' => $request['phone']
        ]);

        Mail::to($user->email)->send(new Welcome($user));

        return response()->json($user, 201);
    }
}
