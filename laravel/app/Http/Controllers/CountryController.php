<?php

namespace App\Http\Controllers;

use App\Http\Resources\CountryResource;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        return CountryResource::collection(
            Country::query()
                ->where('name', 'LIKE', $request['search'] . "%")
                ->orderBy('name')
                ->get()
        );
    }
}
