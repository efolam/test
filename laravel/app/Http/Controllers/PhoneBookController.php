<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePhoneBookRequest;
use App\Http\Requests\UpdatePhoneBookRequest;
use App\Models\PhoneBook;

class PhoneBookController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePhoneBookRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(PhoneBook $phoneBook)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(PhoneBook $phoneBook)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePhoneBookRequest $request, PhoneBook $phoneBook)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(PhoneBook $phoneBook)
    {
        //
    }
}
