import './bootstrap'

import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import VueMask from 'v-mask';

import FormRegister from "./components/forms/Register.vue"

Vue.use(Vuetify)
Vue.use(VueMask)

const app = new Vue({
    el: '#app',
    vuetify: new Vuetify(),
    components: {
        FormRegister,
    },
})
