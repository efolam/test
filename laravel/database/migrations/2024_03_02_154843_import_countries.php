<?php

use App\Models\Country;
use Database\Factories\CountryFactory;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    const PATH = __DIR__ . '/static/countries.json';

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $countries = file_get_contents(self::PATH, true);
        $countries = json_decode($countries);

        foreach ($countries as $country) {
            CountryFactory::new()->create([
                'name' => $country->name,
                'idd'  => $country->idd,
                'flag' => $country->flag,
            ]);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        $countries = file_get_contents(self::PATH, true);
        $countries = json_decode($countries);

        foreach ($countries as $country) {
            $findCountry = Country::query()->where('name', $country->name);
            $findCountry?->delete();
        }
    }
};
